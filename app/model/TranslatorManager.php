<?php

namespace App\Model;

use Nette;


/**
 * This class is used to manage translating
 *
 * @author Lukas Komarek
 */
class TranslatorManager extends Nette\Object
{
	const
		TABLE_TRANSLATOR = "translator",
		
                COLUMN_ID = "id",
		COLUMN_KEY = "key",
		COLUMN_VALUE = "value",
		COLUMN_LANGUAGE = "language";
                

	/** @var Nette\Database\Context */
	private $db;


        public function __construct
        (
                Nette\Database\Context $db
        )
        {
                $this->db = $db;
        }
  
        
        /**
	 * Returns translations
         * @param array $keys
         * @param String $lang
	 * @return array 
	 */
        public function getTranslation($keys, $lang)
        {
                return $this->db
                    ->table(self::TABLE_TRANSLATOR)
                    ->where("key", $keys)
                    ->where("lang", $lang)
                    ->fetchPairs("key","value");
        }
        
}