<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * This class is used to manage users.
 *
 * @author Lukas Komarek
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{
	const
		TABLE_USER = "user",
                
		COLUMN_ID = "id",
		COLUMN_EMAIL = "email",
                COLUMN_PASSWORD_HASH = "password";


	/** @var Nette\Database\Context */
	private $db;

	public function __construct
        (
                Nette\Database\Context $db
        )
	{
		$this->db = $db;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($email, $password) = $credentials;

		$row = $this->db
                    ->table(self::TABLE_USER)
                    ->where(self::COLUMN_EMAIL, $email)
                    ->fetch();

		if (!$row or !Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException("Přihlašovací údaje nejsou správné.", self::IDENTITY_NOT_FOUND);
		}

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}
}