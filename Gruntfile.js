/**
 * Grunt config file 
 */

module.exports = function(grunt) {

	grunt.file.defaultEncoding = 'utf8';
	grunt.file.preserveBOM = false;

	grunt.initConfig({
                
                // concatenate JS libs
                concat: {
                        options: {
                                separator: ';'
                        },
                        dist: {
                                src: ['public_html/js/lib/jquery-2.2.1.min.js', 'public_html/js/lib/*.js'],
                                dest: 'public_html/js/all-concat.js'
                        }
                },
                
                // minify own JS script
                uglify: {
                        my_target: {
                                files: {
                                        'public_html/js/all-min.js': ['public_html/js/all-concat.js']
                                }
                        }
                },
                
                // compile less files to css
                less: {
                        development: {
                                options: {
                                        paths: ["public_html/css"],
                                        cleancss: true
                                },
                                files: {
                                        "public_html/css/less-min.css": "public_html/less/index.less"
                                }
                        }
                },
                
                // minify and concatenate CSS libs
                cssmin: {
                        combine: {
                                files: {
                                        'public_html/css/all-min.css': ['public_html/css/lib/*.css', 'public_html/css/less-min.css']
                                }
                        }
                },
                
                // adds prefixes in css
                autoprefixer: {
                        options: {
                            browsers: ['last 3 versions', 'ie 8', 'ie 9', '> 1%']
                        },
                        main: {
                            expand: true,
                            flatten: true,
                            src: ['public_html/css/all-min.css'],
                            dest: 'public_html/css/'
                        }
                },
                
		// watch for changes and trigger commands
		watch: {
                        concat: {
                                options: {
					livereload: true
				},
				files: ['public_html/js/lib/*.js'],
				tasks: ['concat']
                        },
                        uglify: {
                                options: {
					livereload: true
				},
				files: ['public_html/js/all-concat.js'],
				tasks: ['uglify']
                        },
                        less: {
                                options: {
					livereload: true
				},
				files: ['public_html/less/*/*.less'],
				tasks: ['less']
                        },
                        cssmin: {
                                options: {
					livereload: true
				},
				files: ['public_html/css/lib/*.css', 'public_html/css/less-min.css'],
				tasks: ['cssmin', 'autoprefixer']
                        }
		},
                
                //injecting the changes into all open browsers without a page refresh
                browserSync: {
                        dev: {
                                bsFiles: {
                                        src : ['app/presenters/templates/*/*.latte', 'app/presenters/templates/*/*/*.latte' ,'app/presenters/templates/@layout.latte', 'public_html/css/all-min.css', 'public_html/js/all-min.js', 'public_html/images/*', 'public_html/images/*/*']
                                },
                                options: {
                                    proxy: 'http://my.alliceshop.eadmin.cz', //our PHP server
                                    host: 'my.alliceshop.eadmin.cz',
                                    open: 'external',
                                    watchTask: true
                                }
                        }
                }
	});
        
        // load npm tasks
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-concat');
        grunt.loadNpmTasks('grunt-contrib-cssmin');
        grunt.loadNpmTasks('grunt-contrib-less');
        grunt.loadNpmTasks('grunt-autoprefixer');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-browser-sync');

	// define default task
        grunt.registerTask('default', ["browserSync", "cssmin", "less", "autoprefixer", "uglify", "concat", "watch"]);
        
};