//HERE COMES THE FUNCTIONS
var addOrderToCookie = function(id, sizeNew, sizeOld, quantity, type)
{
    var cart = /cart=([^;]+)/.exec(document.cookie);
    var existedOrder = false;
    var old, count;
    
    $("#costButton").addClass("focus");
    
    setTimeout(function() { 
        $("#costButton").removeClass("focus");
    }, 2000);
    
    if(cart)
    {
        if(type === 'buybutton')
        {
            $.each(cart[1].split(','), function()
            {
                old = this.split('_');
                if((old[0] == id) && old[1] == sizeNew)
                {
                    quantity = parseInt(old[2]) + parseInt(quantity);
                    cart = cart[1].replace(old[0]+'_'+old[1]+'_'+old[2], id+'_'+sizeNew+'_'+quantity);
                    document.cookie = 'cart=' + cart + ';max-age=' + (60*60*24*30);
                    existedOrder = true;
                    return existedOrder;
                }
            });
        }
        else
        {
            var cart = /cart=([^;]+)/.exec(document.cookie);
            cartArray = cart[1].split(",");
            if(cartArray.length > 1 && sizeOld !== sizeNew)
            {
                removeOrderFromCookie(id+"_"+sizeNew+"_1");
            }
            $.each(cart[1].split(','), function()
            {
                old = this.split('_');
                if((old[0] == id) && old[1] == sizeOld)
                {
                    var cart = /cart=([^;]+)/.exec(document.cookie);
                    cart = cart[1].replace(old[0]+'_'+old[1]+'_'+old[2], id+'_'+sizeNew+'_'+quantity);
                    document.cookie = 'cart=' + cart + ';max-age=' + (60*60*24*30);
                    existedOrder = true;
                    return existedOrder;
                }
            });
        }
    }
    if(!existedOrder && type === 'buybutton')
    {
        document.cookie = 'cart=' + (cart ? cart[1] + ',' : '') + id+"_"+sizeNew+"_"+quantity + ';max-age=' + (60*60*24*30);
    }
};

var removeOrderFromCookie = function(order)
{
    var cart = /cart=([^;]+)/.exec(document.cookie);
    cartArray = cart[1].split(",");
    order = order.split("_");
    
    for (i = 0; i < cartArray.length; i++)
    { 
        item = cartArray[i].split("_");
        if(item[0] == order[0] && item[1] == order[1])
        {
            if(cartArray.length == 1)
            {
                cart = cart[1].replace(new RegExp(order[0]+"_"+order[1]+"_.*"), "");
                document.cookie = 'cart=' + cart + ';max-age=' + (60*60*24*30);
                break;
            }
            
            if(cartArray.length == (i+1))
            {
                cart = cart[1].replace(new RegExp(","+order[0]+"_"+order[1]+"_.*"), "");
                document.cookie = 'cart=' + cart + ';max-age=' + (60*60*24*30);
                break;
            }
            
            cart = cart[1].replace(new RegExp(order[0]+"_"+order[1]+"_.*,"), "");
            document.cookie = 'cart=' + cart + ';max-age=' + (60*60*24*30);
            break;
        }
    }
};

var getPriceSum = function()
{
    var priceSum = 0; count = 0; price = 0; order = 0;
    var cart = /cart=([^;]+)/.exec(document.cookie);
 
    if(!cart)
    {
        return 0;
    }
    
    $.each(cart[1].split(','), function() 
    {
        order = this.split('_');
        count = parseInt(order[2]);
        price = parseFloat($("#" + order[0]).parent().find(".price .value").text().replace(/^\s+|\s+$/g, ''));
        priceSum += count * price;
    });
    
    priceSum += parseFloat($("#transportPrice").text());
    
    return formatPrice(priceSum);
};

var getQuantitySum = function()
{
    var productsQuantitySum = 0;
    var cart = /cart=([^;]+)/.exec(document.cookie);
    
    if(!cart)
    {
        return 0;
    }
    
    $.each(cart[1].split(','), function()  {
        productsQuantitySum += parseInt(this.split('_')[2]);
    });
    
    return productsQuantitySum;
};

var getQuantityOfOrder = function(id, size)
{
    var cart = /cart=([^;]+)/.exec(document.cookie);
    var old;
    var quantity = 0;
    
    if(!cart)
    {
        return 0;
    }

    $.each(cart[1].split(','), function()
    {
        old = this.split('_');
        if(old[0] == id && old[1] == size)
        {
            quantity = old[2];
            return;
        }
    });
    
    return parseInt(quantity);
};

var setHeightsOfIframesAndImages = function()
{
    $(".socialsites iframe, .socialsites .owl-video-wrapper").each(function() {
        $(this).height($(this).width());
    });
    $(window).on("resize", function() {
        setTimeout(function() {
            $(".socialsites iframe, .socialsites .owl-video-wrapper").each(function() {
                $(this).height($(this).width());
            });
        }, 1000);
    });
};

var initQuantityButtons = function()
{
    var value, id, size;
    
    $("#plusButton").on("click", function() 
    {    
        value = parseInt($("#quantity").text());
        id = $("#popupProduct .nameofproduct").attr("data-id");
        size = $("#popupProduct .sizeButtonWrap .active").text().replace(/^\s+|\s+$/g, '');
        limitCount = $("#quantity").attr("data-limit-count");
        inCart = getQuantityOfOrder(id, size);
        
        if($("#popupProduct .confirmButton").attr("data-type") === "buybutton")
        {
            if(parseInt($("#quantity").attr("data-limit-final")) === 1 && (value + inCart) >= limitCount)
            {
                alert($("#quantity").attr("data-limit-final-sentence").replace("###", limitCount));
                value = limitCount - inCart;
                $("#quantity").text(value);
            }
            else
            {
                value++;
                $("#quantity").text(value);
            }
        }
        else
        {
            if(parseInt($("#quantity").attr("data-limit-final")) === 1 && value >= limitCount)
            {
                alert($("#quantity").attr("data-limit-final-sentence").replace("###", limitCount));
                $("#quantity").text(limitCount);
            }
            else
            {
                value++;
                $("#quantity").text(value);
            }
        }
    });
            
    $("#minusButton").on("click", function() {
        
        value = parseInt($("#quantity").text())-1;
        
        if(value < 1)
        {
            value = 1;
        }
        
        $("#quantity").text(value);
    });
};

var initSizeButtons = function()
{
    var id, size, index, count;
    $(".sizeButton").on("click", function(){
        $(".sizeButtonWrap").children().removeClass("active");
        $(this).addClass("active");
        id = $("#popupProduct .nameofproduct").attr("data-id");
        size = $(this).find("span").text();
        index = $("#" + id).attr("data-category").split(",").indexOf(size);
        count = $("#" + id).attr("data-limit-count").split(",")[index];
        days = $("#" + id).attr("data-limit-days").split(",")[index];
        final = $("#" + id).attr("data-limit-final").split(",")[index];
        $("#quantity").attr("data-limit-count", count).attr("data-limit-days", days).attr("data-limit-final", final);
    });
};

var showOrder = function(firstTime)
{
    $orders = $("#orders");
    $orders.empty();

    $summary = $("#summaryOrder");
    $summary.show();
    
    $("#totalprice").text(getPriceSum());
    
    var $item, item, html, information, divider, id, picture;
    
    if(getQuantitySum() > 0)
    {
        var cart = /cart=([^;]+)/.exec(document.cookie);
        $.each(cart[1].split(','), function() 
        {
            item = this.split('_');
            $item = $("#"+item[0]);
            picture = $item.parent().find(".media a[data-img]").attr("href");
            
            html = [
                "<div><a href='" + picture + "?noLoop' data-lightbox='" + this + "'><img src='" + picture.replace(".png", "_min.png") + "'></a></div>",
                "<div id=" + this + "><span>" + $item.text() + "</span></div>",
                "<div><span>" + item[1] + "</span></div>",
                "<div><span><span>" + item[2] + "</span> <span>ks</span></span></div>",
                "<div><span>" + formatPrice(parseFloat($item.parent().find('.price .value').text().replace(/^\s+|\s+$/g, ''))) + "</span><span> Kč / ks</span></div>",
                "<div class='deleteButton'>",
                    "<div class='svgWrap'>",
                        "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='25px' height='25px' viewBox='0 0 25 25' enable-background='new 0 0 25 25' xml:space='preserve'>",
                                "<g fill='none' stroke='rgba(255,255,255,0.50)'>",
                                    "<line stroke-width='1.3' stroke-miterlimit='10' x1='0.81' y1='0.746' x2='24.19' y2='24.254'></line>",
                                    "<line stroke-width='1.3' stroke-miterlimit='10' x1='24.19' y1='0.746' x2='0.81' y2='24.254'></line>",
                                "</g>",
                        "</svg>",
                    "</div>",
                "</div>"
            ].join('');

            information = document.createElement('div');
            information.className = 'information';
            information.innerHTML = html;

            divider = document.createElement('div');
            divider.className = "divider";

            $orders.append(information);
            $orders.append(divider);
        });
        
        if(!firstTime)
        {
            $orders.find(".information img").each(function(){
                new RetinaImage(this);
            });
        }
        
        $("#totalpriceButton").text(getPriceSum());
        $("#totalquantity").text(getQuantitySum()).parent().show();
    } 
    else
    {
        $("#form").hide();
        $("#finishButton").show();
        $("#totalpriceButton").text(getPriceSum());
        $("#totalquantity").text(getQuantitySum()).parent().hide();
        $summary.hide();
    }
};

var initConfirmButton = function()
{
    var id, quantity, sizeNew, type;
    
    $(".confirmButton").on("click", function() 
    {
        $nameofproduct = $("#popupProduct .nameofproduct");
        id = $nameofproduct.attr('data-id').replace(/^\s+|\s+$/g, '');
        quantity = parseInt($("#popupProduct .quantity").text().replace(/^\s+|\s+$/g, ''));
        sizeNew = $("#popupProduct .sizeButtonWrap .active").text().replace(/^\s+|\s+$/g, '');
        type = $("#popupProduct .confirmButton").attr("data-type");
        limitCount = $("#quantity").attr("data-limit-count");
        inCart = getQuantityOfOrder(id, sizeNew);
        
        if($("#popupProduct .confirmButton").attr("data-type") === "buybutton")
        {
            if(parseInt($("#quantity").attr("data-limit-final")) === 1 && (inCart+quantity) > limitCount)
            {
                alert($("#quantity").attr("data-limit-final-sentence").replace("###", limitCount));
                quantity = limitCount - inCart;
                $("#quantity").text(quantity);
                return false;
            }
            else
            {
                if(inCart+quantity > limitCount && inCart <= limitCount)
                {
                    addLimitOfTransportDays(id + "_" + sizeNew, $("#quantity").attr("data-limit-days"));
                    transportDays = getTransportDays();
                    $("#transportDays").text(transportDays);
                    alert($("#quantity").attr("data-limit-count-sentence").replace("###", limitCount).replace("$$$", transportDays));
                }
                
                sizeOld = null;
                addOrderToCookie(id, sizeNew, sizeOld, quantity, type);
                showOrder();
                $(".blackWrap, #popupProduct").hide();
                $("body").removeClass("blur");
            }
        }
        else
        {
            if(parseInt($("#quantity").attr("data-limit-final")) === 1 && quantity > limitCount)
            {
                alert($("#quantity").attr("data-limit-final-sentence").replace("###", limitCount));
                $("#quantity").text(limitCount);
                return false;
            }
            else
            {
                if(quantity > limitCount && inCart <= limitCount)
                {
                    addLimitOfTransportDays(id + "_" + sizeNew, $("#quantity").attr("data-limit-days"));
                    transportDays = getTransportDays();
                    $("#transportDays").text(transportDays);
                    alert($("#quantity").attr("data-limit-count-sentence").replace("###", limitCount).replace("$$$", transportDays));
                }
                
                sizeOld = $nameofproduct.attr("data-sizeold");
                
                if(sizeOld !== sizeNew)
                {
                    removeLimitOfTransportDays(id+"_"+sizeOld);
                }
                
                addOrderToCookie(id, sizeNew, sizeOld, quantity, type);
                showOrder();
                $(".blackWrap, #popupProduct").hide();
                $("body").removeClass("blur");
            }
            
            if(getQuantityOfOrder(id, sizeNew) <= limitCount)
            {
                removeLimitOfTransportDays(id + "_" + sizeNew);
                $("#transportDays").text(getTransportDays());
            }
        }
    });
};

var initDeleteButton = function()
{
    $("html").on("click", ".deleteButton .svgWrap", function(){
        var order = $(this).parent().parent().find("div[id]").attr("id");
        removeOrderFromCookie(order);
        showOrder();
        id = order.split("_");
        removeLimitOfTransportDays(id[0] + "_" + id[1]);
        $("#transportDays").text(getTransportDays());
    });
};

var initShowingForm = function()
{
    $(".finishButton").on("click", function(){
        $("#form").show();
        $(".finishButton").hide();
    });
};

var setBackground = function()
{
    if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
    {
        $("body").addClass("background");
    }
};

var smoothScroll = function(pointer, event)
{
    if 
    (
            (location.pathname.replace(/^\//,'') == pointer.pathname.replace(/^\//,''))
            && 
            (location.hostname == pointer.hostname)
            && 
            (getPriceSum() !== 0)
    )
    {
        var target = $(pointer.hash);
        target = target.length ? target : $('[name=' + pointer.hash.slice(1) +']');
        if (target.length && timeoutCostButtonLock) 
        {
            timeoutCostButtonLock = false;
            $('html, body').animate({
                scrollTop: target.offset().top-45
            }, 500, function() {
                setTimeout(function(){ 
                    timeoutCostButtonLock = true;
                }, 500);
            });
            event.preventDefault();
        } 
        else
        {
            $('html, body').clearQueue();
            event.preventDefault();
        }
    }
};

var initSmoothScrolling = function()
{
    $('a[href*="#"]:not([href="#"])').click(function(event) {
        smoothScroll(this, event);
    });
    $(window).on('hashchange', function(event) {
        smoothScroll(this.window.location, event);
    });
};

var initCheckbox = function(name, functionTrue, functionFalse)
{
    $("#" + name).on("click", function() {
        if($("#" + name + "Option").attr( "data-check" )==="false" ) 
        {
            $("#" + name + "Option").prop( "checked" , true);
            $("#" + name + "Option").attr( "data-check" , "true" );
            if (typeof functionTrue == 'function')
            { 
                functionTrue(); 
            }
        } 
        else
        {
            $("#" + name + "Option").prop( "checked" , false);
            $("#" + name + "Option").attr( "data-check" , "false" );
            if (typeof functionFalse == 'function')
            { 
                functionFalse(); 
            }
        }
    });
    $("#" + name).keypress(function(e){
        if(e.keyCode == 13)
        {
            $("#" + name).click();
        }
    });
};

var initCheckboxes = function()
{
    //COMPANY
    initCheckbox("company",
        function() {$( "#companySubForm" ).show(300)},
        function() {$( "#companySubForm" ).hide(300)}
    );
    
    //OTHER ADDRESS
    initCheckbox("otherAddress",
        function() {
            $( "#otherAddressSubForm" ).show(300);
            
            var countries = $("#country_1").attr("data-countries");
            var prices = $("#country_1").attr("data-prices");
            var days = $("#country_1").attr("data-days");
            var val = $("#country_2").val();
            
            $("#country_3").text(val);
            
            countries = countries.split(",");
            prices = prices.split(",");
            days = days.split(",");

            $("#transportPrice").text(prices[countries.indexOf(val)]);
            $("#transportDays").attr("data-limit-country-days", days[countries.indexOf(val)]);
            $("#transportDays").text(getTransportDays());
            $("#totalprice").text(getPriceSum());
        },
        function() {
            $( "#otherAddressSubForm" ).hide(300);
            
            var countries = $("#country_1").attr("data-countries");
            var prices = $("#country_1").attr("data-prices");
            var days = $("#country_1").attr("data-days");
            var val = $("#country_1").val();
            
            $("#country_3").text(val);
            
            countries = countries.split(",");
            prices = prices.split(",");
            days = days.split(",");

            $("#transportPrice").text(prices[countries.indexOf(val)]);
            $("#transportDays").attr("data-limit-country-days", days[countries.indexOf(val)]);
            $("#transportDays").text(getTransportDays());
            $("#totalprice").text(getPriceSum());
        }
    );
    
    //CONDITIONS
    initCheckbox("conditions");
};

var initStatementCookie = function()
{
    var state = /statementCookie=([^;]+)/.exec(document.cookie);
    if(state)
    {
       $("#statementCookie").hide(); 
    }
    else
    {
        setTimeout(function() {
            $("#statementCookie").fadeIn()
            .css({bottom:-45,position:'fixed'})
            .animate({bottom:0}, 1000);
        }, 4000);
        $("#statementCookie .buttonGeneral").on("click", function() {
            document.cookie = 'statementCookie=true;max-age=' + (60*60*24*365);
            $("#statementCookie").fadeOut(300); 
        });
    }
};

var formatPrice = function(num)
{
    var p = num.toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? " " : "") + acc;
    }, "") + (p[1] !== '00' ? "." + p[1] : '');
};

var initOwlCarousel = function()
{
    var owl1 = $( "#owlCarousel1" );
    var owl2 = $( "#owlCarousel2" );
    var owl3 = $( "#owlCarousel3" );
        
    owl1.owlCarousel({
        nav: false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            900:{
                items:3
            }
        },
        onInitialized: function(current) {
            var video = $(current.target).find('video');
            if(video.find("source").attr("src"))
            {
                video.get(0).play();
            }
        },
        autoplay: false,
        lazyLoad: true
    });
    
    owl2.owlCarousel({
        nav: false,
        responsiveClass:true,
        video: true,
        responsive:{
            0:{
                items:1
            },
            400:{
                items:2
            },
            600:{
                items:3
            },
            900:{
                items:4
            }
        },
        onInitialized: setHeightsOfIframesAndImages,
        autoplay: false,
        lazyLoad: true
    });
    
    owl3.owlCarousel({
        nav: false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            }
        },
        onInitialized: function(current) {
            var video = $(current.target).find('video');
            if(video.find("source").attr("src"))
            {
                video.get(0).play();
            }
        },
        autoplay: false,
        lazyLoad: true
    });
};

var initAutoComplete = function()
{
    $("#autocomplete_1").on("focus", geolocate(1));
    $("#autocomplete_2").on("focus", geolocate(2));
};

var initShowingPopup = function()
{
    initShowingPopupBuyButton();
    initShowingPopupInSummary();
    initShowingPopupLangs();
    initShowingPopupChat();
};

var initHidingPopup = function()
{
    $(".blackWrap, #closecross, #endChatButton").on("click", function(){
        $(".blackWrap, #popupProduct, #popupLangs, #popupChat").hide();
        $("body").removeClass("blur");
    });
    
    initHidingPopupChat();
};

var initHidingPopupChat = function()
{
    $("#endChatButton").on("click", function(){
        $("#onlineChatLedge").removeClass("show");
        $("#popupChat > div").removeClass("communication");
    });
};

var initShowingPopupLangs = function()
{
    $("#choosingLang").on("click", function(){
        var lang = $("#choosingLang").text();
        $("#popupLangs .language").each(function(){
            $(this).removeClass("inactive");
        });
        $("#popupLangs .language span:contains('" + lang + "')").parent().addClass("inactive");
        $("#popupLangs").css("top", $(document).scrollTop()+60);
        $("body").addClass("blur");
        $("#popupLangs, .blackWrap").css("display","inline-block");
    });
};

var initShowingPopupChat = function()
{
    $(".onlineChat").on("click", function() {
        $("#popupChat .messages").scrollTop($("#popupChat .messages")[0].scrollHeight);
        $("#popupChat").css("top", $(document).scrollTop()+60);
        $("body").addClass("blur"); 
        $("#popupChat, .blackWrap").css("display","inline-block");
        $("#messages").scrollTop(function() { return this.scrollHeight; });
    });
};

var initShowingPopupBuyButton = function()
{
    $(".buyButton").on("click", function(){
        $("#popupProduct #quantity").text("1");
        $("#popupProduct .sizeButtonWrap").children().removeClass("active");
        $("#popupProduct .sizeButtonWrap div:first-child").addClass("active");
        $("#popupProduct").css("top", $(document).scrollTop()+60);
        var $nameofproduct = $(this).parent().parent().find(".nameofproduct");
        var picture = $nameofproduct.parent().find(".media a[data-img]").attr("href");
        $("#popupProduct .head a").attr("href", picture+"?noLoop");
        $("#popupProduct .head img").attr("src", picture.replace(".png", "_min.png"));
        $("#popupProduct .head img").each(function(){
            new RetinaImage(this);
        });
        var nameofproduct = $nameofproduct.text().replace(/^\s+|\s+$/g, '');
        var priceofproduct = $(this).parent().parent().find(".price .value").text();
        $("#popupProduct .nameofproduct").text(nameofproduct).attr("data-id", $nameofproduct.attr("id")).attr("data-price", priceofproduct);
        $("#popupProduct .nameofproduct").attr("data-sizeold", "");
        var days = $nameofproduct.attr("data-limit-days").split(",")[0];
        var count = $nameofproduct.attr("data-limit-count").split(",")[0];
        var final = $nameofproduct.attr("data-limit-final").split(",")[0];
        $("#quantity").attr("data-limit-days", days).attr("data-limit-count", count).attr("data-limit-final", final);
        $("#popupProduct .head a").attr("data-lightbox", $nameofproduct.attr("id")+"_P");
        $("body").addClass("blur");
        $("#popupProduct .confirmButton").attr("data-type", "buybutton");
        $("#popupProduct, .blackWrap").css("display","inline-block");
    });
};

var initShowingPopupInSummary = function()
{
    $("#orders").on("click", ".information div:nth-child(2) span, .information div:nth-child(3) span, .information div:nth-child(4) > span", function() {
        $thisParent = $(this).parent().parent();
        $("#popupProduct .sizeButtonWrap").children().removeClass("active");
        var size = $thisParent.find("div:nth-child(3)").text();
        $("#popupProduct .sizeButtonWrap .sizeButton span:contains('" + size + "')").parent().addClass("active");
        var quantity = $thisParent.find("div:nth-child(4) span span:first-child").text();
        $("#popupProduct #quantity").text(quantity);
        $("#popupProduct").css("top", $(document).scrollTop()+60);
        $nameofproduct = $("#popupProduct .nameofproduct");
        var picture = $thisParent.find("div:first-child a").attr("href");
        $("#popupProduct .head a").attr("href", picture);
        $("#popupProduct .head img").attr("src", picture.replace(".png?noLoop", "_min.png"));
        $("#popupProduct .head img").each(function(){
            new RetinaImage(this);
        });
        $nameofproduct.text($thisParent.find("div:nth-child(2) span").text().replace(/^\s+|\s+$/g, ''));
        var id = $thisParent.find("div:nth-child(2)").attr("id").split("_")[0];
        $nameofproduct.attr("data-id", id);
        $nameofproduct.attr("data-sizeold", size);
        $nameofproduct.attr("data-price", $thisParent.find("div:nth-child(5) span:first-child").text());
        var index = $("#"+id).attr("data-category").split(",").indexOf(size);
        var count = $("#"+id).attr("data-limit-count").split(",")[index];
        var days = $("#"+id).attr("data-limit-days").split(",")[index];
        var final = $("#"+id).attr("data-limit-final").split(",")[index];
        $("#quantity").attr("data-limit-count", count).attr("data-limit-days", days).attr("data-limit-final", final);
        $("#popupProduct .head a").attr("data-lightbox", $nameofproduct.attr("data-id")+"_P");
        $("body").addClass("blur");        
        $("#popupProduct .confirmButton").attr("data-type", "summary");
        $("#popupProduct, .blackWrap").css("display","inline-block");
    });
};

var getTransportDays = function()
{
    var days = $("#transportDays").attr("data-limit-product-days").split(",");
                
    if(days.length > 0) 
    {
        days = parseInt($("#transportDays").attr("data-limit-country-days")) + parseInt(Math.max.apply(null, days));
    }
    else
    {
        days = $("#transportDays").attr("data-limit-country-days");
    }
    
    return days;
};

var removeLimitOfTransportDays = function(id)
{
    ids = $("#transportDays").attr("data-limit-product-id").split(",");
    days = $("#transportDays").attr("data-limit-product-days").split(",");
    index = ids.indexOf(id);
    if(index > -1)
    {
        ids.splice(index,1);
        days.splice(index,1);
        $("#transportDays").attr("data-limit-product-id", ids.join(","));
        $("#transportDays").attr("data-limit-product-days", days.join(","));
        document.cookie = 'data-limit-product-id=' + ids.join(",") + ';max-age=' + (60*60*24*30);
        document.cookie = 'data-limit-product-days=' + days.join(",") + ';max-age=' + (60*60*24*30);
    }
};

var addLimitOfTransportDays = function(id, days)
{
    lastIds = $("#transportDays").attr("data-limit-product-id");
    lastDays = $("#transportDays").attr("data-limit-product-days");
    if(lastDays !== "")
    {
        lastDays = lastDays + ",";
        lastIds = lastIds + ",";
    }
    $("#transportDays").attr("data-limit-product-days", lastDays + days).attr("data-limit-product-id", lastIds + id);
    document.cookie = 'data-limit-product-id=' + lastIds + id + ';max-age=' + (60*60*24*30);
    document.cookie = 'data-limit-product-days=' + lastDays + days + ';max-age=' + (60*60*24*30);
};

var loadLimitsFromCookie = function()
{
    var productIds = /data-limit-product-id=([^;]+)/.exec(document.cookie);
    var productDays = /data-limit-product-days=([^;]+)/.exec(document.cookie);
    
    if(productIds)
    {
        $("#transportDays")
        .attr("data-limit-product-id", productIds[1])
        .attr("data-limit-product-days", productDays[1]);

        $("#transportDays").text(getTransportDays());
    }
};

var initTextareaAutoheight = function()
{
    var observe;
    if (window.attachEvent) {
        observe = function (element, event, handler) {
            element.attachEvent('on'+event, handler);
        };
    }
    else {
        observe = function (element, event, handler) {
            element.addEventListener(event, handler, false);
        };
    }

    var text = document.getElementById('textareaChat');
    function resize () {
        text.style.height = '35px';
        text.style.height = (text.scrollHeight)+'px';
    }
    /* 0-timeout to get the already changed text */
    function delayedResize () {
        window.setTimeout(resize, 0);
    }
    observe(text, 'change',  resize);
    observe(text, 'cut',     delayedResize);
    observe(text, 'paste',   delayedResize);
    observe(text, 'drop',    delayedResize);
    observe(text, 'keydown', delayedResize);

    text.focus();
    text.select();
    resize();
};