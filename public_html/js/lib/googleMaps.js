// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete_1, autocomplete_2;
var componentForm = 
{
    street_number: 'short_name',
    locality: 'long_name',
    route: 'long_name',
    country: 'long_name',
    administrative_area_level_1: 'short_name',
    postal_code: 'short_name'
};

function initAutocomplete() 
{
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    var countries = $("body").attr('data-countries').split(",");
    var options = 
    {
          types: ['geocode'],
          componentRestrictions: {country: countries}
    };
  
    autocomplete_1 = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete_1')),
      options);
    
    autocomplete_2 = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete_2')),
      options);

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete_1.addListener('place_changed',  function() 
    { 
        fillInAddress(autocomplete_1, 1) 
    });
    autocomplete_2.addListener('place_changed',  function() 
    { 
        fillInAddress(autocomplete_2, 2) 
    });
}

function fillInAddress(autocomplete, $num) 
{
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    var countries = $("#country_1").attr("data-countries");
    var prices = $("#country_1").attr("data-prices");
    var days = $("#country_1").attr("data-days");
    var sentence = $("#country_1").attr("data-sentence");
    var transportDays;

    countries = countries.split(",");
    prices = prices.split(",");
    days = days.split(",");

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) 
    {
        var addressType = place.address_components[i].types[0];

        if (componentForm[addressType]) 
        {
            var val = place.address_components[i][componentForm[addressType]];
            
            if (addressType == "country") 
            {
                $("#country_" + $num).val(val);
                $("#country_3").text(val);
                $("#transportPrice").text(prices[countries.indexOf(val)]);
                $("#transportDays").attr("data-limit-country-days", days[countries.indexOf(val)]);
                
                transportDays = getTransportDays();
                
                $("#transportDays").text(transportDays);
                $("#totalprice").text(getPriceSum());
                
                if(val != countries[0])
                {
                    alert(sentence.replace("###", transportDays).replace("$$$", prices[countries.indexOf(val)]));
                }
            }
            
            if (addressType == "locality") {
                $("#locality_" + $num).val(val);
            }
            
            if (addressType == "postal_code") {
                $("#locality_" + $num).val($("#locality_" + $num).val() + ", " + val);
            }
        }
    }
  
    var route = $("#autocomplete_" + $num).val().split(",");
    var street;
    
    for (var i = 0; i < route.length-2; i++) 
    {
        if(i == 0)
        {
            street = route[i];
        } else 
        {
            street += ", " + route[i];
        }
    }
    
    $("#autocomplete_" + $num).val(street);
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate($num) 
{
    if (navigator.geolocation) 
    {
        navigator.geolocation.getCurrentPosition(function(position) 
        {
            var geolocation = 
            {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            if($num === 1) 
            {
                autocomplete_1.setBounds(circle.getBounds());
            } 
            else 
            {
                autocomplete_2.setBounds(circle.getBounds());
            }
        });
    }
}