//HERE COMES THE GLOBAL VARS
var timeoutCostButtonLock = true;


//HERE COMES THE CORE
$(document).ready(function()
{
    initSmoothScrolling();
    initShowingPopup();
    initHidingPopup();
    initQuantityButtons();
    initSizeButtons();
    initConfirmButton();
    initDeleteButton();
    initShowingForm();
    initLightboxLoop();
    setBackground();
    showOrder("firstTime");
    initCheckboxes();
    initStatementCookie();
    initOwlCarousel();
    initAutoComplete();
    loadLimitsFromCookie();
    initTextareaAutoheight();
});

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
{
    $(window).on("load", function()
    {
        showOrder();
        initCheckboxes();
        initStatementCookie();
        initOwlCarousel();
        loadLimitsFromCookie();
        initTextareaAutoheight();
    });
};
